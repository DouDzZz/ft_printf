/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bindec.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 15:11:17 by fldoucet          #+#    #+#             */
/*   Updated: 2019/03/04 17:26:31 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static	int	ft_power(int nb, int pow)
{
	int ret;

	ret = nb;
	if (pow == 0)
		return (1);
	while (pow > 1)
	{
		ret = ret * nb;
		pow--;
	}
	return (ret);
}

int			ft_bindec(char *bin)
{
	int		ret;
	int		i;
	int		j;

	j = 0;
	i = ft_strlen(bin) - 1;
	ret = 0;
	while (bin[i])
	{
		ret = ret + ((bin[i] - 48) * ft_power(2, j));
		j++;
		i--;
	}
	return (ret);
}
