/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 15:26:17 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/18 15:07:17 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char		*ft_itoa(int n)
{
	int		i;
	long	nb;
	int		neg;
	char	*res;

	if (!(res = (char*)malloc(sizeof(char) * (ft_intlen((int)n) + 1))))
		return (NULL);
	neg = n < 0;
	i = 0;
	ft_bzero(res, (ft_intlen((int)n) + 1));
	nb = neg ? (long)n * -1 : (long)n;
	if (nb == 0)
		res[i] = '0';
	while (nb)
	{
		res[i] = nb % 10 + 48;
		nb = nb / 10;
		i++;
	}
	if (neg)
		res[i] = '-';
	ft_strrev(res);
	res[ft_intlen((int)n) + 1] = '\0';
	return (res);
}
