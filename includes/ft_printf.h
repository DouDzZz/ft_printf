/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 17:24:38 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/04 16:35:08 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stddef.h>
# include "../libft/includes/libft.h"

/*
** BONUS HEADERS
*/

# include <locale.h>

/*
** END BONUS HEADER
*/

typedef struct	s_lprintf
{
	char				*special;
	char				*field;
	char				*precision;
	char				*dig_pos_pre;
	char				*modifier;
	char				*conversion;
	char				*percent;
	char				*to_swap;
	void				*arg;
	int					fd;
	int					error;
	double				f;
	long double			lf;
	struct s_lprintf	*next;
}				t_lprintf;

int				ft_printf(const char *restrict format, ...);
int				ft_init(char *percent, t_lprintf **list, void *arg);
int				ft_init_float(char *percent,
								t_lprintf **list, double args);
int				ft_init_float_long(char *percent,
								t_lprintf **list, long double args);
void			handle_char(t_lprintf *list);
void			handle_binary(t_lprintf *list);
void			handle_string(t_lprintf *list);
void			handle_digit(t_lprintf *list);
void			handle_base(t_lprintf *list);
void			handle_long(t_lprintf *list);
void			handle_hexa_long(t_lprintf *list);
void			handle_float(t_lprintf *list);
void			handle_float_long(t_lprintf *list);
void			handle_octal_long(t_lprintf *list);
void			handle_padding(t_lprintf *list);
void			handle_unsigned(t_lprintf *list);
void			handle_unsigned_long(t_lprintf *list);
void			padd_digit(t_lprintf **list, char *arg, int field, int left);
void			print_field(int field, t_lprintf **list);
int				ft_parse(t_lprintf **list, char *str);
void			ft_check(t_lprintf **list);
int				ft_choose(char *str, t_lprintf **list);
int				ft_color(char *str, t_lprintf **list, int start);
t_lprintf		*ft_create_argument_list(void);
void			ft_lst_del(t_lprintf **list);
void			handle_padding(t_lprintf *list);
void			handle_title(t_lprintf *list);
void			ft_add(char c, char ***title);
void			ft_add_pqr(char c, char ***title);
int				ft_strlen_swap(t_lprintf *list);
unsigned char	*handle_unicode(wchar_t unicode);
int				ft_strlenuni(wchar_t *str);
int				ft_25line(char c);

#endif
